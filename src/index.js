import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

function Square(props) {
  return (
    <button className="square" onClick={props.onClick}>
      {props.value}
    </button>
  );
}

class Grid extends React.Component {
  renderSquare(v, r, c) {
    return <Square value={v} onClick={() => this.props.onClick(r, c)} />;
  }

  renderBoardRow(currentGridRow, r) {
    const gridRow = [];
    for (let c = 0; c < currentGridRow.length; c++)
      gridRow.push(this.renderSquare(currentGridRow[c], r, c));

    return <div className="board-row">{gridRow}</div>;
  }

  render() {
    const currentGrid = this.props.gridStatus;
    const gridData = [];
    for (let r = 0; r < currentGrid.length; r++)
      gridData.push(this.renderBoardRow([...currentGrid[r]], r));

    return gridData;
  }
}

class Board extends React.Component {
  constructor(props) {
    super(props);
    const size = Number.parseInt(this.props.size);
    this.state = {
      grid: Array(size).fill(Array(size).fill(null)),
      nextPlayer: "X",
    };

    this.togglePlayer = this.togglePlayer.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.checkWinnerVerdict = this.checkWinnerVerdict.bind(this);
  }

  togglePlayer() {
    this.setState({ nextPlayer: this.state.nextPlayer === "X" ? "0" : "X" });
  }

  checkWinnerVerdict() {
    const grid = JSON.parse(JSON.stringify(this.state.grid));
    const size = grid.length;
    for (let r = 0; r < size; r++) {
      for (let c = 0; c < size; c++) {
        const e1 = grid[r][c];
        if (!e1) continue;
        let rSt = c === 0,
          cSt = r === 0;
        let m = rSt ? c + 1 : 0;
        let n = cSt ? r + 1 : 0;
        if (!rSt && !cSt) break;
        for (; m < size && n < size; m++, n++) {
          const e2 = grid[r][m];
          const e3 = grid[n][c];

          if (c === 0 && rSt && (!e2 || e1 !== e2)) rSt = false;
          if (r === 0 && cSt && (!e3 || e1 !== e3)) cSt = false;

          if (!rSt && !cSt) break;
        }

        if (m === size && rSt) return grid[r][m - 1];
        if (n === size && cSt) return grid[n - 1][c];
      }
    }

    const tl_br = grid[0][0];
    if (tl_br) {
      let d = 1;
      for (; d < size; d++) if (grid[d][d] !== tl_br) break;
      if (d === size) return tl_br;
    }

    const tr_bl = grid[0][size - 1];
    if (tr_bl) {
      let d = 1;
      for (; d < size; d++) if (grid[d][size - d - 1] !== tr_bl) break;
      if (d === size) return tr_bl;
    }

    return false;
  }

  checkWinnerGrid3() {
    const grid = JSON.parse(JSON.stringify(this.state.grid));
    for (let r = 0; r < grid.length; r++) {
      if (grid[r][0] === grid[r][1] && grid[r][0] === grid[r][2])
        return grid[r][2];
      if (grid[0][r] === grid[1][r] && grid[0][r] === grid[2][r])
        return grid[2][r];
    }
    if (grid[0][0] === grid[1][1] && grid[0][0] === grid[2][2])
      return grid[0][0];
    if (grid[0][2] === grid[1][1] && grid[0][2] === grid[2][0])
      return grid[0][2];
  }

  handleClick(r, c) {
    const updateGrid = JSON.parse(JSON.stringify(this.state.grid));
    if (updateGrid[r][c] || this.checkWinnerVerdict()) return;
    updateGrid[r][c] = this.state.nextPlayer;
    this.setState({ grid: updateGrid });
    this.togglePlayer();
  }

  resetGame(size) {
    this.setState({
      grid: Array(size).fill(Array(size).fill(null)),
      nextPlayer: "X",
    });
  }

  render() {
    const next = this.state.nextPlayer;
    const winner = this.checkWinnerVerdict();
    if (winner) {
      alert(`Player ${winner} wins..!`);
    }

    return (
      <div>
        <div>
          <button
            className="resetGame"
            onClick={() => this.resetGame(this.props.size)}
          >
            New Game
          </button>
        </div>
        <div className="status">Next Player : {next} </div>
        <div className="grid">
          <Grid gridStatus={this.state.grid} onClick={this.handleClick} />
        </div>
      </div>
    );
  }
}
class Game extends React.Component {
  render() {
    return (
      <div className="game">
        <div className="game-board">
          <Board size={4} />
        </div>
        <div className="game-info">
          <ol>{/* TODO */}</ol>
        </div>
      </div>
    );
  }
}

ReactDOM.render(<Game />, document.getElementById("root"));
